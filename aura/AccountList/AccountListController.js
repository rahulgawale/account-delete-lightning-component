({
    doInit: function(component, event, helper) {      
        // Fetch the account list from the Apex controller   
        helper.getAccountList(component);
        /*
       * doInit : function(component, event) {
        var action = component.get("c.findAll");
        action.setCallback(this, function(a) {
            component.set("v.accounts", a.getReturnValue());
        });
        $A.enqueueAction(action);
    }
      */
  },
    deleteAccount: function(component, event, helper) {
        // Prevent the form from getting submitted
        event.preventDefault();
        
        // Get the value from the field that's in the form
        var accountName = event.target.getElementsByClassName('account-name')[0].value;
        var accountId = event.target.getElementsByClassName('account-id')[0].value;
        confirm('Delete the ' + accountName + ' account?');
        helper.deleteAccount(component,accountId);
    }
})