({
    // Fetch the accounts from the Apex controller
    getAccountList: function(component) {
        var action = component.get('c.getAccounts');
        
        // Set up the callback
        var self = this;
        action.setCallback(this, function(actionResult) {
            component.set('v.accounts', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    deleteAccount: function(component, accountId1){
        console.log('in helper delete'+ accountId1);
        var action = component.get("c.deleteAccount1");
        console.log(action);
        action.setParams({
            "accountId":accountId1
        });
        
        action.setCallback(this, function(response) {
            console.log('in callback');

            let state = response.getState();
            if (state === "SUCCESS") {
                let toastParams = {
                    title: "Success",
                    message: "Account Deleted", // Default error message
                    type: "success"
                };
				var toastEvent = $A.get("e.force:showToast");
				if(toastEvent){
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                } else {
                    alert(toastParams.title + ': '+ toastParams.message);
                }
                this.getAccountList(component);
            } else if (state === "ERROR") {
	
                let toastParams = {
                    title: "Error",
                    message: "Unknown error",
                    type: "error"
                };
                let errors = response.getError();
				console.log('errors',errors);
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    toastParams.message = errors[0].pageErrors[0].message;
                }
                // Fire error toast
                var toastEvent = $A.get("e.force:showToast");
                if(toastEvent){
                    toastEvent.setParams(toastParams);
                    toastEvent.fire();
                } else {
                    alert(toastParams.title + ': '+ toastParams.message + '\n\n'+ errors[0].message);
                }
            }
        });
        $A.enqueueAction(action);
    }
})