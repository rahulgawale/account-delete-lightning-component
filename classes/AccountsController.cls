public class AccountsController {
    @AuraEnabled
    public static List<Account> getAccounts() {
        return [SELECT Id, name, industry, Type, NumberOfEmployees, TickerSymbol, Phone
                FROM Account ORDER BY createdDate ASC];
    }
    
    @AuraEnabled 
    public static boolean deleteAccount1(Id accountId){
        system.debug('in delete');

			Account acc = [SELECT Id FROM Account WHERE id =: accountId];
			delete acc;
			return true;            
    }
}